// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyAoWeGJ6WhTBNMS8m190S1OQG2juc7mB9k",
    authDomain: "biblioteca-56e62.firebaseapp.com",
    projectId: "biblioteca-56e62",
    storageBucket: "biblioteca-56e62.appspot.com",
    messagingSenderId: "935016337347",
    appId: "1:935016337347:web:ea75b22f92dcfcb38c3776"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
