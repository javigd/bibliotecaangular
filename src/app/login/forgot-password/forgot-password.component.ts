import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {

  constructor(public authService: AuthService) { }
  usuario ={
    email: ''
  }
  mensaje = "";
  ngOnInit(): void {
  }
  SendEmail(){
    this.mensaje="Su email se ha enviado correctamente";
    const {email} = this.usuario;
    this.authService.ForgotPassword(email);
  }
}
