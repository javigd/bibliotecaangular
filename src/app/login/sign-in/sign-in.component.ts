import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../servicios/auth.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  usuario = {
    email: '',
    password: ''
  }
  constructor(public authService: AuthService, private router:Router) { }

  signIn(){
    const {email, password} = this.usuario;
    this.authService.login(email, password);
    this.router.navigate(['/portada']);
  }
  ngOnInit(): void {
  }

}
