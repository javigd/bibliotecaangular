import { Component, OnInit } from '@angular/core';
import { AuthService } from '../servicios/auth.service';

@Component({
  selector: 'app-verify-email',
  templateUrl: './verify-email.component.html',
  styleUrls: ['./verify-email.component.css']
})
export class VerifyEmailComponent implements OnInit {

  constructor(private authService: AuthService) { }

  userLogged = this.authService.getUserLogged();

  ngOnInit(): void {
  }
  resend(){
    this.authService.SendVerificationMail();
  }
}
