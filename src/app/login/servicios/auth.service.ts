import { Injectable, NgZone } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;
  userVerified:any;


  constructor(public afs: AngularFirestore, public afAuth: AngularFireAuth,
    private router: Router,public ngZone: NgZone) { }


  async login(email:string, password:string){
    try{
      return await this.afAuth.signInWithEmailAndPassword(email,password).then((result)=>{
        this.getUserLogged().subscribe((result) =>{
          this.userVerified =  (result !== null && result?.emailVerified) ? true : false;
          this.router.navigate(['/portada']);
        })
      });
    }catch(err){
      console.log("------------------"  + err);
      return null;
    }
  }

  async register(email:string, password:string){
    try{
      return await this.afAuth.createUserWithEmailAndPassword(email,password).then(()=>{
        this.SendVerificationMail();
        this.logout();
      })
    }catch(err){
      console.log("------------------"  + err);
      return null;
    }
  }

  SendVerificationMail() {
    return this.afAuth.currentUser.then(u => u?.sendEmailVerification())
    .then(() => {
      this.router.navigate(['/verifyEmail']);
    })
  }

  getUserLogged(){
    return this.afAuth.authState;
  }

  logout(){
    this.afAuth.signOut().then(() => {
      this.router.navigate(['/login']);
    })
  }

  ForgotPassword(email:string) {
    return this.afAuth.sendPasswordResetEmail(email)
  }

 get isLoggedIn():boolean{
   try{
    this.getUserLogged().subscribe(result => {
      this.userVerified = result;
    })
  }catch(error){
    return false;
  }
    return (this.userVerified !== null && this.userVerified?.emailVerified) ? true : false;
  }
}
