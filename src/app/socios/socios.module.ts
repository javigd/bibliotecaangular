import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SociosRoutingModule } from './socios-routing.module';
import { DialogData2, ListaSociosComponent } from './componentes/lista-socios/lista-socios.component';
import { ResxsocDialog, SocioDetalleComponent } from './componentes/socio-detalle/socio-detalle.component';
import { FilterPipe } from './pipes/filter.pipe';
import { EditSocioComponent } from './componentes/edit-socio/edit-socio.component';
import { NuevoSocioComponent } from './componentes/nuevo-socio/nuevo-socio.component';



@NgModule({
  declarations: [
    ListaSociosComponent,
    SocioDetalleComponent,
    FilterPipe,
    DialogData2,
    EditSocioComponent,
    NuevoSocioComponent,
    ResxsocDialog
  ],
  imports: [
    CommonModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule,
    SociosRoutingModule,
  ]
})
export class SociosModule { }
