import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class SociosService {

  constructor(private firestore:AngularFirestore) { }

  public createSocio(data: {apellidos:string, nombre:string, avatar:string, email:string, edad:number, telefono:string}){
    return this.firestore.collection('socios').add(data);
  }

  public getSocio(documentId:string){
    return this.firestore.collection('socios').doc(documentId).snapshotChanges();
  }

  public getSocios(){
    return this.firestore.collection('socios').snapshotChanges();
  }

  public updateSocio(documentId:string, data:any){
    return this.firestore.collection('socios').doc(documentId).set(data);
  }
  public deleteSocio(documentId:string){
    return this.firestore.collection('socios').doc(documentId).delete();
  }
}
