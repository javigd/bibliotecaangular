import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any) {
    return value.filter((socio: { data: { nombre: string; apellidos:string; telefono:string; email:string; };
    }) =>
    socio.data.nombre.toLowerCase().indexOf(arg) != -1 || socio.data.email.toLowerCase().indexOf(arg) != -1 ||
    socio.data.apellidos.toLowerCase().indexOf(arg) != -1 || socio.data.telefono.indexOf(arg) != -1)
  }

}
