import { Component, Inject, OnInit } from '@angular/core';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import { SociosService } from '../../servicio/socios.service';
import { PrestamosService } from 'src/app/prestamos/servicios/prestamos.service';

@Component({
  selector: 'app-lista-socios',
  templateUrl: './lista-socios.component.html',
  styleUrls: ['./lista-socios.component.css']
})
export class ListaSociosComponent implements OnInit {

  public socios?:any = [];
  filterSocio = "";

  constructor(private socioService:SociosService, public dialog:MatDialog) {   }

  openDelete(id:string, nombre:string, apellidos:string){
    this.dialog.open(DialogData2,{
      data:{codigo:id, nombree:nombre, surname:apellidos},
    });
  }
  ngOnInit(): void {
    this.socioService.getSocios().subscribe((socioSnapshot) => {
      this.socios = [];
      socioSnapshot.forEach((socioData:any) =>{
      this.socios.push({
        id:socioData.payload.doc.id,
        data:socioData.payload.doc.data()
      });
      });
  });
  }
}
@Component({
  selector: 'dialog-data2',
  templateUrl: 'dialog-data2.html',
})
export class DialogData2 implements OnInit{
  constructor( private socioService:SociosService, public dialogRef: MatDialogRef<DialogData2>, @Inject(MAT_DIALOG_DATA) public data: any, private prestamoService:PrestamosService) {}

  prestamos:any = [];
  reservado:boolean = false;

ngOnInit(): void {
  this.prestamoService.getPrestamos().subscribe((prestamoSnapshot) => {
    this.prestamos = [];
    prestamoSnapshot.forEach((prestamoData: any) => {
      this.prestamos.push({
        id: prestamoData.payload.doc.id,
        data: prestamoData.payload.doc.data(),
      });
      for (let i = 0; i < this.prestamos.length; i++) {
        if (this.prestamos[i].data.idSocio == this.data.codigo) {
          this.reservado = true;
          break;
        }
      }
    });
  });
}

  onNoClick(): void {
    this.dialogRef.close();
  }
  deleteUser(id:string):void{
    this.socioService.deleteSocio(id).then(() => {
      console.log('Documento eliminado!');
    }, (error) => {
      console.error(error);
    });
    this.onNoClick();
  }
}

