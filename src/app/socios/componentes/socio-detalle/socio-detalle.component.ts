import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Params } from '@angular/router';
import { LibrosService } from 'src/app/libros/servicio/libros.service';
import { PrestamosModule } from 'src/app/prestamos/prestamos.module';
import { PrestamosService } from 'src/app/prestamos/servicios/prestamos.service';
import { SociosService } from '../../servicio/socios.service';

@Component({
  selector: 'app-socio-detalle',
  templateUrl: './socio-detalle.component.html',
  styleUrls: ['./socio-detalle.component.css']
})
export class SocioDetalleComponent implements OnInit {

  id?:any;
  socioSeleccionado:any;
  constructor(private socioService:SociosService, private ruta:ActivatedRoute, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.ruta.params.subscribe((params:Params) => {this.id = params['id']});
    this.socioService.getSocio(this.id).subscribe((socioData) =>{
      this.socioSeleccionado = socioData.payload.data();
    })
  }
  verReservas(){
    const dialogRef = this.dialog.open(ResxsocDialog,{
      data:{
        codigo: this.id,
        nombreCompleto: this.socioSeleccionado.nombre + " " + this.socioSeleccionado.apellidos,
      }
    });
  }
}
@Component({
  selector: 'resxsocDialog',
  templateUrl: 'resxsoc-dialog.html',
})
export class ResxsocDialog implements OnInit{

  prestamos:any = [];
  socioSeleccionado:any;
  libros:any = [];

  constructor( public dialogRef: MatDialogRef<ResxsocDialog>, @Inject(MAT_DIALOG_DATA) public data: any, private prestamoService:PrestamosService, private socioService:SociosService, private libroService:LibrosService){}

  ngOnInit(): void {
    this.prestamoService.getPrestamos().subscribe((prestamoSnapshot:any) => {

      this.prestamos = [];
      prestamoSnapshot.forEach((prestamoData: any) =>{
        this.prestamos.push({
          id: prestamoData.payload.doc.id,
          data: prestamoData.payload.doc.data(),
        });
      });
    });
    this.libroService.getBooks().subscribe((libroSnapshot:any) => {
      this.libros = [];
      libroSnapshot.forEach((librosData: any) =>{
        this.libros.push({
          id: librosData.payload.doc.id,
          data: librosData.payload.doc.data()
        });
      });
    });
    this.socioService.getSocio(String(this.data.codigo)).subscribe((socioData:any) => {
      this.socioSeleccionado = socioData.payload.data();
    })
  }

  relacion(idPrimaria:String, idDependiente:String): Boolean{
    return Boolean (String (idPrimaria.trim()) == String (idDependiente.trim()));
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
