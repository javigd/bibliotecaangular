import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { SociosService } from '../../servicio/socios.service';

@Component({
  selector: 'app-nuevo-socio',
  templateUrl: './nuevo-socio.component.html',
  styleUrls: ['./nuevo-socio.component.css']
})
export class NuevoSocioComponent implements OnInit {

  public documentId = null;
  public newSocioForm = new FormGroup({
    id : new FormControl(""),
    nombre :  new FormControl("", Validators.required),
    apellidos :  new FormControl("",Validators.required),
    email :  new FormControl("", Validators.required),
    edad : new FormControl("", Validators.required),
    avatar :  new FormControl("", Validators.required),
    telefono: new FormControl("", Validators.required)
  })

  constructor(private socioService:SociosService,private router:Router) {
    this.newSocioForm.setValue({
      id:"",
      nombre:"",
      apellidos:"",
      email:"",
      edad:"",
      avatar:"",
      telefono:""
    })
  }

  ngOnInit(): void {
  }
  public createSocio(form: { nombre: any; apellidos: any; telefono: any; email: any; edad: any; avatar: any; }, documentId = this.documentId) {
    let data = {
      nombre: form.nombre,
      apellidos: form.apellidos,
      telefono: form.telefono,
      email: form.email,
      edad:form.edad,
      avatar: form.avatar,
    }
    this.socioService.createSocio(data).then(() => {
      console.log('Documento creado con éxito!');
      this.newSocioForm.setValue({
        nombre: '',
        apellidos: '',
        avatar: '',
        email:"",
        edad:"",
        telefono:""
      });
    }, (error) => {
      console.error(error);
    });
    this.router.navigate(["/socios"]);
}
}
