import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { SociosService } from '../../servicio/socios.service';

@Component({
  selector: 'app-edit-socio',
  templateUrl: './edit-socio.component.html',
  styleUrls: ['./edit-socio.component.css']
})
export class EditSocioComponent implements OnInit {

  socioSeleccionado:any;
  editForm:any;
  id:any;
  constructor(private socioService:SociosService, private ruta:ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.ruta.paramMap.subscribe((params:ParamMap) =>{
      this.id = (<string>params.get('id'));
      console.log(this.id);
      this.socioService.getSocio(this.id).subscribe((socioData:any)=>{
        this.socioSeleccionado = socioData.payload.data();
        this.editForm = new FormGroup({
          nombre: new FormControl(this.socioSeleccionado.nombre, Validators .required),
          apellidos: new FormControl(this.socioSeleccionado.apellidos, Validators.required),
          telefono: new FormControl(this.socioSeleccionado.telefono, Validators.required),
          email: new FormControl(this.socioSeleccionado.email, Validators.required),
          edad: new FormControl(this.socioSeleccionado.edad, Validators.required),
          avatar: new FormControl(this.socioSeleccionado.avatar, Validators.required),
        })
      });
    })
  }
  editSocio(){
    if (this.editForm.valid){
      this.socioService.updateSocio(this.id, this.editForm.value);
    }
    this.router.navigate(['/socios']);
  }

}
