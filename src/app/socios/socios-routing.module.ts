import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditSocioComponent } from './componentes/edit-socio/edit-socio.component';
import { ListaSociosComponent } from './componentes/lista-socios/lista-socios.component';
import { NuevoSocioComponent } from './componentes/nuevo-socio/nuevo-socio.component';
import { SocioDetalleComponent } from './componentes/socio-detalle/socio-detalle.component';

const routes: Routes = [

  {path:"", component:ListaSociosComponent},
  {path:"nuevoSocio", component: NuevoSocioComponent},
  {path:"editSocio/:id", component:EditSocioComponent},
  {path:"socioDetalle/:id", component:SocioDetalleComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SociosRoutingModule { }
