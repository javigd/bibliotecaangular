import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AngularFireModule } from '@angular/fire/compat';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PrestamosRoutingModule } from './prestamos-routing.module';
import { DeletePrestamoDialog, ListaPrestamosComponent } from './componentes/lista-prestamos/lista-prestamos.component';
import { AngularMaterialModule } from '../angular-material/angular-material.module';



@NgModule({
  declarations: [
    ListaPrestamosComponent,
    DeletePrestamoDialog
  ],
  imports: [
    CommonModule,
    AngularFireModule,
    FormsModule,
    ReactiveFormsModule,
    PrestamosRoutingModule,
    AngularMaterialModule,
  ]
})
export class PrestamosModule { }
