import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListaPrestamosComponent } from './componentes/lista-prestamos/lista-prestamos.component';

const routes: Routes = [

  {path:"", component:ListaPrestamosComponent},
  //{path:"______", component:},
  //{path:"_____/:id", component:},
  //{path:"____/:id", component:},

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
export class PrestamosRoutingModule { }
