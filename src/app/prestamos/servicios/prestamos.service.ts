import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/compat/firestore';

@Injectable({
  providedIn: 'root'
})
export class PrestamosService {

  constructor(private firestore:AngularFirestore) { }

  public createPrestamo(data: {idLibro:string, idSocio:string, fechaInicio:Date, fechaVencimiento:Date, fechaDevolucion:any}){
    return this.firestore.collection('prestamos').add(data);
  }

  public getPrestamo(documentId:string){
    return this.firestore.collection('prestamos').doc(documentId).snapshotChanges();
  }

  public getPrestamos(){
    return this.firestore.collection('prestamos').snapshotChanges();
  }

  public updatePrestamos(documentId:string, data:any){
    return this.firestore.collection('prestamos').doc(documentId).set(data);
  }
  public deletePrestamo(documentId:string){
    return this.firestore.collection('prestamos').doc(documentId).delete();
  }
}
