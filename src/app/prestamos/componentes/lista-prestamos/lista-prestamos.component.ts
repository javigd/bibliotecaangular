import { Component, Inject, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { LibrosService } from 'src/app/libros/servicio/libros.service';
import { SociosService } from 'src/app/socios/servicio/socios.service';
import { PrestamosService } from '../../servicios/prestamos.service';

@Component({
  selector: 'app-lista-prestamos',
  templateUrl: './lista-prestamos.component.html',
  styleUrls: ['./lista-prestamos.component.css'],
})
export class ListaPrestamosComponent implements OnInit {
  prestamos: any = [];
  libros:any = [];
  socios:any = [];
  fechaHoy:Date = new Date();
  constructor(private prestamoService: PrestamosService, private socioService:SociosService, private libroService:LibrosService, private dialog:MatDialog) {}

  ngOnInit(): void {
    this.prestamoService.getPrestamos().subscribe((prestamoSnapshot:any) => {

      this.prestamos = [];
      prestamoSnapshot.forEach((prestamoData: any) =>{
        this.prestamos.push({
          id: prestamoData.payload.doc.id,
          data: prestamoData.payload.doc.data(),
        });
      });
    });
    this.libroService.getBooks().subscribe((libroSnapshot:any) => {
      this.libros = [];
      libroSnapshot.forEach((librosData: any) =>{
        this.libros.push({
          id: librosData.payload.doc.id,
          data: librosData.payload.doc.data()
        });
      });
    });
    this.socioService.getSocios().subscribe((socioSnapshot:any) => {
      this.socios = [];
      socioSnapshot.forEach((sociosData: any) =>{
        this.socios.push({
          id: sociosData.payload.doc.id,
          data: sociosData.payload.doc.data()
        });
      });
    });
  }

  relacion(idPrimaria:String, idDependiente:String): Boolean{
    return Boolean (String (idPrimaria.trim()) == String (idDependiente.trim()))
  }
  prorroga(idPrestamo:string){
    let prestamoSeleccionado:any;
    let fechaProrroga: Date = new Date();
    fechaProrroga.setDate(fechaProrroga.getDate() + 15);
    let editSubscribe = this.prestamoService.getPrestamo(idPrestamo).subscribe((prestamoData) =>{
      prestamoSeleccionado = prestamoData.payload.data();
      prestamoSeleccionado.fechaVencimiento = fechaProrroga;
      this.prestamoService.updatePrestamos(idPrestamo,prestamoSeleccionado);
      editSubscribe.unsubscribe();
    })
  }
  eliminarPrestamo(idPrestamo:string){
    const dialogRef = this.dialog.open(DeletePrestamoDialog,{
      data:{
        codigo: idPrestamo,
      }
    });
  }
}
@Component({
  selector: 'deletprestamo-dialog',
  templateUrl: 'delete-prestamo.html',
})
export class DeletePrestamoDialog{

  prestamoSeleccionado:any = [];
  libroSeleccionado:any = [];
  constructor(private prestamoService:PrestamosService, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<DeletePrestamoDialog>, private libroService:LibrosService){}

  deletePrestamo():void{
    let fechaDevolucion:Date = new Date();
   let editSub1 = this.prestamoService.getPrestamo(this.data.codigo).subscribe((prestamoData) =>{
      this.prestamoSeleccionado = prestamoData.payload.data();
      let editSub2 = this.libroService.getBook(this.prestamoSeleccionado.idLibro).subscribe((libroData) => {
        this.libroSeleccionado = libroData.payload.data();
        this.libroSeleccionado.prestado = false;
        this.libroService.updateBook(this.prestamoSeleccionado.idLibro, this.libroSeleccionado);
        editSub2.unsubscribe();
      })
      this.prestamoSeleccionado.fechaDevolucion = fechaDevolucion;
      this.prestamoService.updatePrestamos(this.data.codigo, this.prestamoSeleccionado);
      editSub1.unsubscribe();
    })
    this.onNoClick();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}
