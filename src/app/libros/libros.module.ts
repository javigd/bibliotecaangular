import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DialogData, ListaLibrosComponent } from './componentes/lista-libros/lista-libros.component';
import { LibrosRoutingModule } from './libros-routing.module';
import { AngularMaterialModule } from '../angular-material/angular-material.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilterPipe } from './pipes/filter.pipe';
import { NuevoLibroComponent } from './componentes/nuevo-libro/nuevo-libro.component';
import { EditarLibroComponent } from './componentes/editar-libro/editar-libro.component';
import { LibroDetalleComponent, ReservaDialog } from './componentes/libro-detalle/libro-detalle.component';



@NgModule({
  declarations: [
    ListaLibrosComponent,
    FilterPipe,
    NuevoLibroComponent,
    DialogData,
    EditarLibroComponent,
    LibroDetalleComponent,
    ReservaDialog
  ],
  imports: [
    CommonModule,
    LibrosRoutingModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class LibrosModule { }
