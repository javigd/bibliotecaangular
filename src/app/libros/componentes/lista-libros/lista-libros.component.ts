import { Component, Inject, OnInit } from '@angular/core';
import {
  MatDialog,
  MatDialogRef,
  MAT_DIALOG_DATA,
} from '@angular/material/dialog';
import { PrestamosService } from 'src/app/prestamos/servicios/prestamos.service';
import { LibrosService } from '../../servicio/libros.service';

@Component({
  selector: 'app-lista-libros',
  templateUrl: './lista-libros.component.html',
  styleUrls: ['./lista-libros.component.css'],
})
export class ListaLibrosComponent implements OnInit {
  public libros: any = [];
  filterBook = '';

  constructor(private libroSerevice: LibrosService, public dialog: MatDialog) {}

  openDelete(id: String, titulo: String) {
    this.dialog.open(DialogData, {
      data: { codigo: id, title: titulo },
    });
  }

  ngOnInit(): void {
    this.libroSerevice.getBooks().subscribe((libroSnapshot) => {
      this.libros = [];
      libroSnapshot.forEach((libroData: any) => {
        this.libros.push({
          id: libroData.payload.doc.id,
          data: libroData.payload.doc.data(),
        });
      });
    });
  }
}
@Component({
  selector: 'dialog-data',
  templateUrl: 'dialog-data.html',
})
export class DialogData implements OnInit {
  constructor(
    private libroService: LibrosService,
    public dialogRef: MatDialogRef<DialogData>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private prestamoService: PrestamosService
  ) {}

  reservado: boolean = false;
  prestamos: any = [];

  ngOnInit(): void {
    this.prestamoService.getPrestamos().subscribe((prestamoSnapshot) => {
      this.prestamos = [];
      prestamoSnapshot.forEach((prestamoData: any) => {
        this.prestamos.push({
          id: prestamoData.payload.doc.id,
          data: prestamoData.payload.doc.data(),
        });
        for (let i = 0; i < this.prestamos.length; i++) {
          if (this.prestamos[i].data.idLibro == this.data.codigo) {
            this.reservado = true;
            break;
          }
        }
      });
    });
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  deleteLibro(id: string): void {
    this.libroService.deleteBook(id).then(
      () => {
        console.log('Documento eliminado!');
      },
      (error) => {
        console.error(error);
      }
    );
    this.onNoClick();
  }
}
