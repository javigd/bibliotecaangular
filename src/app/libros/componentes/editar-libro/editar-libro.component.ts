import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { LibrosService } from '../../servicio/libros.service';

@Component({
  selector: 'app-editar-libro',
  templateUrl: './editar-libro.component.html',
  styleUrls: ['./editar-libro.component.css']
})
export class EditarLibroComponent implements OnInit {

  libroSeleccionado:any;
  editForm:any;
  id:any;
  generos: any[] = [
    {value: 'Narrativa Española', viewValue: 'Narrativa Española'},
    {value: 'Narrativa Extranjera', viewValue: 'Narrativa Extranjera'},
    {value: 'Filosofía', viewValue: 'Filosofía'},
    {value: 'Psicología', viewValue: 'Psicología'},
  ];
  constructor(private libroService:LibrosService, private ruta:ActivatedRoute, private router:Router) { }

  ngOnInit(): void {
    this.ruta.paramMap.subscribe((params:ParamMap) =>{
      this.id = (<string>params.get('id'));
      this.libroService.getBook(this.id).subscribe((libroData:any)=>{
        this.libroSeleccionado = libroData.payload.data();
        this.editForm = new FormGroup({
          titulo: new FormControl(this.libroSeleccionado.titulo, Validators .required),
          ISBN: new FormControl(this.libroSeleccionado.ISBN, Validators.required),
          autor: new FormControl(this.libroSeleccionado.autor, Validators.required),
          imagen: new FormControl(this.libroSeleccionado.imagen, Validators.required),
          genero: new FormControl(this.libroSeleccionado.genero),
          descripcion: new FormControl(this.libroSeleccionado.descripcion, Validators.required),
          prestado: new FormControl(false)
        })
      });
    })
  }

  editLibro(){
    if (this.editForm.valid){
      this.libroService.updateBook(this.id, this.editForm.value);
    }
    this.router.navigate(['/libros']);
  }
}

