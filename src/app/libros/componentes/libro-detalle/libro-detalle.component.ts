import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, ParamMap, Params } from '@angular/router';
import { PrestamosService } from 'src/app/prestamos/servicios/prestamos.service';
import { SociosService } from 'src/app/socios/servicio/socios.service';
import { LibrosService } from '../../servicio/libros.service';

@Component({
  selector: 'app-libro-detalle',
  templateUrl: './libro-detalle.component.html',
  styleUrls: ['./libro-detalle.component.css']
})
export class LibroDetalleComponent implements OnInit {

  libroSeleccionado:any;
  id?:any;
  constructor(private libroService:LibrosService, private ruta:ActivatedRoute, public dialog: MatDialog) { }

  ngOnInit(): void {
    this.ruta.params.subscribe((params:Params) => {this.id = params['id']});
    this.libroService.getBook(this.id).subscribe((libroData) =>{
      this.libroSeleccionado = libroData.payload.data();
    })
  }
  reservarLibro(){
    const dialogRef = this.dialog.open(ReservaDialog,{
      data:{
        codigo: this.id,
      }
    });
  }
}
@Component({
  selector: 'reserva-dialog',
  templateUrl: 'reserva-dialog.html',
})
export class ReservaDialog implements OnInit{

  socios:any = [];
  public newReserva = new FormGroup({
    idSocio:new FormControl("", Validators.required),
  });
  libroSeleccionado:any;
  constructor(private socioService:SociosService, private libroService:LibrosService,private prestamoService:PrestamosService, @Inject(MAT_DIALOG_DATA) public data: any, public dialogRef: MatDialogRef<ReservaDialog>){}

  ngOnInit():void{
    this.socioService.getSocios().subscribe((socioSnapshot) => {
      this.socios = [];
      socioSnapshot.forEach((socioData:any) =>{
      this.socios.push({
        id:socioData.payload.doc.id,
        data:socioData.payload.doc.data()
      });
      });
  });
  }
  crearReserva(form: { idLibro:string, idSocio:string}){
    let fechaVencimiento: Date = new Date();
    let fechaInicio:Date = new Date();
    let fechaDevolucion:any = null;
    fechaVencimiento.setDate(fechaVencimiento.getDate() + 10)
    this.prestamoService.createPrestamo({idLibro:this.data.codigo, idSocio:form.idSocio, fechaInicio, fechaVencimiento, fechaDevolucion});
    let editSubscribe = this.libroService.getBook(this.data.codigo).subscribe((libroData) =>{
      this.libroSeleccionado = libroData.payload.data();
      this.libroSeleccionado.prestado = true;
      this.libroService.updateBook(this.data.codigo, this.libroSeleccionado);
      editSubscribe.unsubscribe();
    })
    this.onNoClick();
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
}


