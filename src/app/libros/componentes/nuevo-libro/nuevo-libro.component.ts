import { generateForwardRef } from '@angular/compiler/src/render3/util';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, MinLengthValidator, MinValidator, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LibrosService } from '../../servicio/libros.service';

@Component({
  selector: 'app-nuevo-libro',
  templateUrl: './nuevo-libro.component.html',
  styleUrls: ['./nuevo-libro.component.css']
})
export class NuevoLibroComponent implements OnInit {

  public documentId = null;
  public newBookForm = new FormGroup({
    prestado:new FormControl(""),
    id : new FormControl(""),
    titulo :  new FormControl("", Validators.required),
    autor :  new FormControl("", Validators.required),
    genero :  new FormControl("", Validators.required),
    descripcion : new FormControl("", Validators.required),
    imagen :  new FormControl("", Validators.required),
    ISBN: new FormControl("", Validators.required)
  })

  generos: any[] = [
    {value: 'Narrativa Española', viewValue: 'Narrativa Española'},
    {value: 'Narrativa Extranjera', viewValue: 'Narrativa Extranjera'},
    {value: 'Filosofía', viewValue: 'Filosofía'},
    {value: 'Psicología', viewValue: 'Psicología'},
  ];
  constructor(private libroService:LibrosService, private router:Router) {
    this.newBookForm.setValue({
      id:"",
      titulo:"",
      autor:"",
      genero:"",
      descripcion:"",
      imagen:"",
      ISBN:"",
      prestado:"",
    })
  }
  ngOnInit(): void {
  }
  public createBook(form: { titulo: any; imagen: any; autor: any; genero: any; descripcion: any; ISBN: any; prestado:boolean;}, documentId = this.documentId) {
      let data = {
        titulo: form.titulo,
        imagen: form.imagen,
        autor: form.autor,
        genero: form.genero,
        descripcion:form.descripcion,
        ISBN: form.ISBN,
        prestado:false,
      }
      this.libroService.createBook(data).then(() => {
        console.log('Documento creado con éxito!');
        this.newBookForm.setValue({
          titulo: '',
          imagen: '',
          autor: '',
          descripcion:"",
          ISBN:"",
          genero:"",
          prestado:"",
        });
      }, (error) => {
        console.error(error);
      });
      this.router.navigate(["/libros"]);
  }
}
