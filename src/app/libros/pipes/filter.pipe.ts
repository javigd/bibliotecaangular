import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform(value: any, arg: any): any {
    return value.filter((book: { data: { titulo: string; autor:string; genero:string; ISBN:string; };
    }) =>
    book.data.titulo.toLowerCase().indexOf(arg) != -1 || book.data.autor.toLowerCase().indexOf(arg) != -1 ||
    book.data.genero.toLowerCase().indexOf(arg) != -1 || book.data.ISBN.toLowerCase().indexOf(arg) != -1)
  }
}
