import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/compat/firestore';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LibrosService {

  constructor(private firestore:AngularFirestore) { }


  public createBook(data: {titulo:string, autor:string, ISBN:string, descripcion:string, genero:string, imagen:string}){
    return this.firestore.collection('libros').add(data);
  }

  public getBook(documentId:string){
    return this.firestore.collection('libros').doc(documentId).snapshotChanges();
  }

  public getBooks(){
    return this.firestore.collection('libros').snapshotChanges();
  }

  public updateBook(documentId:string, data:any){
    return this.firestore.collection('libros').doc(documentId).set(data);
  }
  public deleteBook(documentId:string){
    return this.firestore.collection('libros').doc(documentId).delete();
  }
}
