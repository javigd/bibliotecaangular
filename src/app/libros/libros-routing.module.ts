import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EditarLibroComponent } from './componentes/editar-libro/editar-libro.component';
import { LibroDetalleComponent } from './componentes/libro-detalle/libro-detalle.component';
import { ListaLibrosComponent } from './componentes/lista-libros/lista-libros.component';
import { NuevoLibroComponent } from './componentes/nuevo-libro/nuevo-libro.component';


const routes: Routes = [

  {path:"", component:ListaLibrosComponent},
  {path:"nuevoLibro", component: NuevoLibroComponent},
  {path:"editLibro/:id", component:EditarLibroComponent},
  {path:"libroDetalle/:id", component:LibroDetalleComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibrosRoutingModule { }
