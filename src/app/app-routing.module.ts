import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ErrorPageComponent } from './componentesInicio/error-page/error-page.component';
import { HomePageComponent } from './componentesInicio/home-page/home-page.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { AuthGuard } from './login/guard/auth.guard';
import { SignInComponent } from './login/sign-in/sign-in.component';
import { SignUpComponent } from './login/sign-up/sign-up.component';
import { VerifyEmailComponent } from './login/verify-email/verify-email.component';

const routes: Routes = [

  {path:"libros", loadChildren: () => import("./libros/libros.module").then(m => m.LibrosModule), canActivate:[AuthGuard]},
  {path:"socios", loadChildren:() => import("./socios/socios.module").then(m => m.SociosModule), canActivate:[AuthGuard]},
  {path:"prestamos", loadChildren:() => import("./prestamos/prestamos.module").then(m => m.PrestamosModule), canActivate:[AuthGuard]},
  {path:"portada", component:HomePageComponent, canActivate:[AuthGuard]},
  {path:"login", component:SignInComponent},
  {path:"verifyEmail", component:VerifyEmailComponent},
  {path:"forgotPassword", component:ForgotPasswordComponent},
  {path:"signUp", component:SignUpComponent},
  {path:"", component:SignInComponent},
  {path:"**", component:ErrorPageComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
