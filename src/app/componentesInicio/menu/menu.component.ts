import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/login/servicios/auth.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private authService:AuthService) { }

  userLogged = this.authService.getUserLogged();
  
  ngOnInit(): void {
  }
  logout(){
    this.authService.logout();
  }
}
